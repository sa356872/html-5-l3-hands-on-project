# HTML-5-L3-Hands On Project

This is a sample HTML-5 L3 Hands On Project

Requirements:

Launch it on a browser which supports WebWorker

Goal:

Takes a user input number whcih is <=100 in a form and increments and checks whether the number is prime. If the number is prime it terminates or else continues
If user presses "End" the process ends and it shows the last incremented number or the last incremented  number is Prime, whichever comes first


On Pressing Start

Stores the number in localStorage as "UserInput"

,In the worker thread,

Checks the number if its prime 

If the number is prime, stops the worker thread and tells the number is prime

else continues to increment and then again check for prime

Once prime number is detected it terminates the Worker Thread

On Pressing End, 

Ends the the Worker Thread and it shows the last incremented number or the last incremented number is Prime, whichever comes first


Steps:

1. Please launch a node server or a python local server in the repo directory where the code is cloned

2. After successfully creating a server, launch the http://localhost:8000/HandsOnHTML5-L3.html page from Chrome or a Browser which supports Web Worker

3. Use the Project with above Steps and try it out



